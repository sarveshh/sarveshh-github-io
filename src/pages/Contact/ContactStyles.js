import { styled } from "@mui/material/styles";
import { Typography, Paper, Box } from "@mui/material";

export const ContactContainer = styled("div")(({ theme }) => ({
  display: "flex",
  margin: "30px 100px",
  flexDirection: "column",
}));
